import Card from './Card.js';
import Game from './Game.js';
import SpeedRate from './SpeedRate.js';

// Отвечает является ли карта уткой.
function isDuck(card) {
    return card && card.quacks && card.swims;
}

// Отвечает является ли карта собакой.
function isDog(card) {
    return card instanceof Dog;
}
function isLad(card) {
    return card instanceof Lad && Lad.prototype.hasOwnProperty('modifyDealedDamageToCreature') && Lad.prototype.hasOwnProperty('modifyTakenDamage');
}
function isRogue(card) {
    return card instanceof Rogue;
}
function isBrewer(card) {
    return card instanceof Brewer;
}
function isNemo(card) {
    return card instanceof Nemo;
}
// Дает описание существа по схожести с утками и собаками
function getCreatureDescription(card) {
    const description = ['Существо'];
    if (isDuck(card) && isDog(card)) {
        description[0] = 'Утка-Собака';
    }
    if (isDuck(card)) {
        description[0] = 'Утка';
    }
    if (isDog(card)) {
        description[0] = 'Собака';
    }
    if (isLad(card)) {
        description.push('Чем их больше, тем они сильнее');
    }
    if (isRogue(card)) {
        description.push('Воруй-убивай. Гусей нет? Сгодятся утки.');
    }
    if (isBrewer(card)) {
        description.push('Пивка для рывка!');
    }
    if (isNemo(card)) {
        description.push('The one without a name without an honest heart as compass');
    }
    return description;
}


class Creature extends Card {
    constructor(...args) {
        super(...args);
        // this._currentPower = this.maxPower;
    }
    // set currentPower(value) {
    //     if (this._currentPower < value)
    //         this.view.signalHeal();
    //     this._currentPower = value <= this.maxPower ? value : this.maxPower;
    // }
    // get currentPower() {
    //     return this._currentPower;
    // }
    getDescriptions() {
        return [super.getDescriptions(), ...getCreatureDescription(this)];
    }
}
// Основа для утки.
class Duck extends Creature {
    constructor(name = 'Пес-бандит', maxPower = 3, image) {
        super('Мирная утка', 2);
    }
    quacks = () => { console.log('quack') };
    swims = () => { console.log('float: both;') };
}
// Основа для собаки.
class Dog extends Creature {
    constructor(name = 'Пес-бандит', maxPower = 3, image) {
        super(name, maxPower, image);
    }
    swims = () => { console.log('float: none;') };
}
class Lad extends Dog {
    constructor() {
        super('Браток', 2);
    }
    static getInGameCount() {
        return this.inGameCount || 0;
    };
    static setInGameCount(value) {
        this.inGameCount = value;
    };
    static getBonus() {
        const count = this.getInGameCount();
        return count * (count + 1) / 2;
    }

    doAfterComingIntoPlay(...args) {
        super.doAfterComingIntoPlay(...args);
        Lad.setInGameCount(Lad.getInGameCount() + 1);
    }
    doBeforeRemoving(...args) {
        super.doBeforeRemoving(...args);
        Lad.setInGameCount(Lad.getInGameCount() - 1);
    }
    modifyDealedDamageToCreature(...args) {
        console.log(this, `+${Lad.getBonus()} к силе`);
        super.modifyDealedDamageToCreature(args[0] + Lad.getBonus(), ...args.slice(1));
    }
    modifyTakenDamage(...args) {
        console.log(this, `+${Lad.getBonus()} к защите`);
        super.modifyTakenDamage(args[0] - Lad.getBonus(), ...args.slice(1));
    }
    static inGameCount = 0;
}
class Rogue extends Creature {
    constructor() {
        super('Изгой', 2);
        this.stolenAbilities = { dealedDamage: [], takenDamage: [] };
    }
    modifyDealedDamageToCreature(value, toCard, gameContext, continuation) {
        const toCardProto = Object.getPrototypeOf(toCard);
        if (toCardProto.hasOwnProperty('modifyDealedDamageToCreature')) {
            this.stolenAbilities.dealedDamage.push(toCardProto.modifyDealedDamageToCreature);
            delete toCardProto.modifyDealedDamageToCreature;
        }
        if (toCardProto.hasOwnProperty('modifyTakenDamage')) {
            this.stolenAbilities.takenDamage.push(toCardProto.modifyTakenDamage);
            delete toCardProto.modifyTakenDamage;
        }
        gameContext.updateView();
        this.stolenAbilities.dealedDamage.forEach(abil => abil.call(this, ...arguments));
    }
    modifyTakenDamage(...args) {
        this.stolenAbilities.takenDamage.forEach(abil => abil.call(this, ...args));
    }
}
class Brewer extends Duck {
    constructor() {
        super('Пивовар', 2);
    }
    doBeforeAttack(gameContext, continuation) {
        const cardList = gameContext.currentPlayer.table.concat(gameContext.oppositePlayer.table);
        cardList.forEach(card => {
            if (isDuck(card)) {
                card.maxPower++;
                card.currentPower = card.currentPower + 2 <= card.maxPower ? card.currentPower + 2 : card.maxPower;
            }
        });
        gameContext.updateView();
        super.doBeforeAttack(gameContext, continuation);
    }
}
class Nemo extends Creature {
    constructor() {
        super('Немо', 4);
    }
    modifyDealedDamageToCreature(value, toCard, gameContext, continuation) {
        Object.setPrototypeOf(this, Object.getPrototypeOf(toCard));
        super.doBeforeAttack(gameContext, continuation);
        gameContext.updateView();
        this.modifyDealedDamageToCreature(value, toCard, gameContext, continuation);
    }
}
console.log(new Creature())
// Колода Шерифа, нижнего игрока.
const seriffStartDeck = [
    new Nemo(),
    new Duck(),
];

// Колода Бандита, верхнего игрока.
const banditStartDeck = [
    new Brewer(),
    new Lad()
];


// Создание игры.
const game = new Game(seriffStartDeck, banditStartDeck);

// Глобальный объект, позволяющий управлять скоростью всех анимаций.
SpeedRate.set(1);

// Запуск игры.
game.play(false, (winner) => {
    alert('Победил ' + winner.name);
});
